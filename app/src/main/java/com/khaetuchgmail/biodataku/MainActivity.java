package com.khaetuchgmail.biodataku;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input);

        }
}

//        public class MainActivity extends AppCompatActivity {
//
//            /**
//             *   membuat variable Button, saya buat dengan nama "button"
//             */
//
//            private Button button = (Button)findViewById(R.id.button);
//
//            @Override
//            protected void onCreate(Bundle savedInstanceState) {
//                super.onCreate(savedInstanceState);
//                setContentView(R.layout.activity_main);
//
//                button.setOnClickListener(new View.OnClickListener() {
//
//                    @Override
//                    public void onClick(View view) {
//
//                        /**STEP A
//                         *
//                         *    "i" adalah variable Intent
//                         *    kemudian kita akan memanggil method Intent dengan perintah New Intent()
//                         *    memasukan parameter Intent() dengan MainActivity kemudian ActivityTujuan
//                         */
//
//                        Intent i = new Intent(MainActivity.this, InputActivity.class);
//
//
//
//                        /**STEP B
//                         *
//                         *    memulai Aktifitas perintah yang dibuat di bagian A
//                         *    yaitu "i"
//                         *    dengan method startActivity()
//                         */
//
//                        startActivity(i);
//
//                    }
//                });
//            }
//        }

//        Animation anim_clockwise = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.clockwise);
//        ImageView logo_launcher = findViewById(R.id.logo_launcher);
//
//        anim_clockwise.setAnimationListener(new Animation.AnimationListener() {
//            @Override
//            public void onAnimationStart(Animation animation) {
//
//            }
//
//            @Override
//            public void onAnimationEnd(Animation animation) {
//                startActivity(new Intent(getApplicationContext(), InputActivity.class));
//            }
//
//            @Override
//            public void onAnimationRepeat(Animation animation) {
//
//            }
//        });
//        logo_launcher.startAnimation(anim_clockwise);
